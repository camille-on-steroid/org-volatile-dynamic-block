;;; org-volatile-dynamic-block.el --- Org dynamic blocks whose contents are never saved -*- lexical-binding:t -*-

;; Copyright (C) 2023 Camille Le Bon

;; Author: Camille Le Bon <camille.lebon35000@gmail.com>
;; Version: 0.1
;; Keywords: org, blocks, convenience
;; URL: https://gitlab.com/camille-on-steroid/org-volatile-dynamic-block

;;; Commentary:

;; This package provides a macro to define org dynamic-block write functions
;; that do not set the modified flag of the buffer when the block's contents are
;; updated.  Moreover, a hook empties the contents of the block before saving
;; the buffer into a file.  In other words, this package provides a macro to
;; define volatile org dynamic blocks.

;;; Code:
(require 'org)

(defvar-local org-volatile-dynamic-block-buffer--modified-p nil
   "Variable set to `buffer-modified-p' before calling `org-prepare-dblock'.")

(defun org-volatile-dynamic-block--clear (block-name)
  "Clear the contents of the org dynamic block BLOCK-NAME."
  (save-mark-and-excursion
    (org-find-dblock block-name)
    (org-prepare-dblock)))

(defun org-volatile-dynamic-block--get-modified-p ()
  "Call `buffer-modified-p' and store the result for use after save."
  (setq org-volatile-dynamic-block-buffer--modified-p (buffer-modified-p)))

(defun org-volatile-dynamic-block--restore-modified ()
  "Restore the modified buffer flag saved when calling `org-prepare-dblock'."
  (set-buffer-modified-p org-volatile-dynamic-block-buffer--modified-p))

(advice-add 'org-prepare-dblock
	    :before #'org-volatile-dynamic-block--get-modified-p)

;;;###autoload
(defmacro define-org-volatile-dynamic-block (name arglist &rest body)
  "Define a new volatile orgmode dynamic block.

A volatile dynamic block is a dynamic block which content is not meant to be
saved and an update of said block must not be interpreted by Emacs as a
modification of the buffer.  One should see a volatile dynamic block as a
fully dynamic and volatile part of an orgmode file.

A volatile dynamic block is a function, it has a name - the name of the block as
used in orgmode - and arguments.  The arguments of the function are mapped to
the plist of the block parameters in the org buffer.  For example, one can
define a volatile dynamic block with the following code:

  (define-org-volatile-dynamic-block my-cool-block (myname age)
    (insert (concat \"My name is \" myname
                    \", and I am \" (int-to-string age)
                    \" years old.\")))

To use it in orgmode, one can write the following block:

  #+begin: my-cool-block :myname \"Camille\" :age 30
  #+end

Upon calling \\[org-update-dblock], the function \\+`org-dblock-write:my-cool-block'
 is called and the block contents would be updated and display:

  My name is Camille, and I am 30 years old.

Remember nonetheless that these are orgmode dynamic blocks, which means that the
arguments of the function are the parameters added to the block header, as shown
above.  Consequently, some parameter names are reserved by orgmode and cannot be
used for the user's argument names, as they would be overriden. Typically, the
following symbols cannot be used:

  :name               - the name of the dynamic block
  :content            - the content of the dynamic block
  :indentation-column - the indentation column

This macro also creates a function \\+`org-volatile-dblock-create:my-cool-block'
function which inserts a dynamic block named \\+`my-cool-block' with each needed
parameter set to a default value into the current buffer.  The dynamic block
type is defined using `org-dynamic-block-define' for the user to be able to
insert a volatile dynamic buffer using \\[org-dynamic-block-insert-dblock].

\\(fn NAME (ARGLIST) BODY...)"
  (declare (indent 2))

  (unless (symbolp name)
    (signal 'wrong-type-argument (list 'symbolp name)))
  (unless (or (eql arglist nil) (consp arglist))
    (signal 'wrong-type-argument (list 'consp arglist)))

  (cl-labels ((arg-key (arg) (intern (concat ":" (symbol-name arg)))))

    (let* ((func-name (symbol-name name))
	   (update-func-name (intern (concat "org-dblock-write:" func-name)))
	   (create-func-name (intern (concat "org-volatile-dblock-create:"
					     func-name)))
	   (arg-plist (flatten-list
		       (mapcar (lambda (arg) (list (arg-key arg) ""))
			       arglist))))
      `(progn
	 (defun ,create-func-name ()
	   (interactive)
	   (org-create-dblock
	    (org-combine-plists ',arg-plist '(:name ,func-name))))

	 (org-dynamic-block-define ,func-name #',create-func-name)

	 (defun ,update-func-name (params)
	   (let ,(mapcar (lambda (arg)
			   (cons arg `((plist-get params ,(arg-key arg)))))
			 arglist)
	     ,@body
	     (add-hook 'before-save-hook
		       (lambda ()
			 (org-volatile-dynamic-block--clear ,func-name))
		       nil t)
	     (org-volatile-dynamic-block--restore-modified)))))))

;;; org-volatile-dynamic-block.el ends here
